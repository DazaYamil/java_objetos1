package MercadoDeObjetos;

public interface ShippingMethod {
    String getDescription();
}
