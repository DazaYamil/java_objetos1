package MercadoDeObjetos;

public interface PaymentMethod {
    String getDescription();
}
