package Ejercicio15;

public class CuadroTarifario {
    private double precioKwh;

    public CuadroTarifario() {
    }

    public void setPrecioKwh(double precioKwh) {
        this.precioKwh = precioKwh;
    }

    public double getPrecioKwh() {
        return this.precioKwh;
    }
}
