package Parciales.ResolucionParcial02;

public interface Rol {

    public double valorBase(Personaje p);
    public void incrementarHabilidades(Personaje p);
}
