package Parciales.ResolucionParcial01;

public class Sede {
    private String nombre;
    private double precioPorDia;
    private int cantidadDias;

    public Sede(String nombre, double precioPorDia, int cantidadDias){
        this.nombre = nombre;
        this.precioPorDia = precioPorDia;
        this.cantidadDias = cantidadDias;
    }
}
