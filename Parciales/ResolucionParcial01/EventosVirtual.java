package Parciales.ResolucionParcial01;

import java.time.LocalDate;

public class EventosVirtual extends Evento{
    public EventosVirtual(String nombre, LocalDate fecha, String temaPrincipal, double precioInscripcion, double precioRemera){
        super(nombre, fecha, temaPrincipal, precioInscripcion, precioRemera);
    }
}
