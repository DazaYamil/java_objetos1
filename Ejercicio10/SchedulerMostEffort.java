package Ejercicio10;

import java.util.List;

public class SchedulerMostEffort extends JobScheduler{
    public SchedulerMostEffort(String strategy) {
        super(strategy);
    }

    public JobDescription jobStrategy(List<JobDescription> jobs) {
        return (JobDescription)jobs.stream().max((j1, j2) -> {
            return Double.compare(j1.getEffort(), j2.getEffort());
        }).orElse((JobDescription) null);
    }
}
